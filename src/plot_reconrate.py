import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib
import argparse
import itertools

rc('font', **{'family': 'serif', 'serif': ['Computer Modern'], 'size': 14})
rc('text', usetex=True)

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int)
parser.add_argument("--nref", nargs='+')
parser.add_argument("--Re", type=float)
parser.add_argument("--Rem", type=float)
parser.add_argument("--RH", type=float, nargs='+')
args, _ = parser.parse_known_args()

Re = args.Re
Rem = args.Rem
RH = args.RH
baseN = args.baseN
nrefs = args.nref
legendvec = []
linestyles = itertools.cycle(['--', ':', '-', '-.', (0, (3, 1, 1, 1))])

for nref in nrefs:
    for rh in RH:
        outputdir = "./results/"
        Nx = int(float(baseN)*2**float(nref))
        filename = "reconrate_{0:}_{1:}_{2:1.7f}_{3:}x{4:}".format(int(float(Rem)), int(float(Re)), float(rh), Nx, Nx)

        with open(outputdir+filename+".txt", 'r') as f:
            data = f.read().split("\n")[0:-1]

        ts = []
        rates = []

        for dat in data:
            t, rate = dat.split("&")
            ts.append(float(t))
            rates.append(float(rate))

        plt.plot(ts, rates, linestyle=next(linestyles), linewidth=2)
#        legendvec.append("{0:}x{1:}".format(Nx, Nx))
        legendvec.append(r"$\mathrm{R_H}=\,$"+"{0:}".format(rh))
    
plt.xlabel(r"$t$")
plt.ylabel("reconnection rate")
plt.ylim([-0.000, 0.037])
plt.legend(legendvec, loc="upper right")
plt.title(r"$\mathrm{Re_m}=\mathrm{Re}=\,$" +f"{int(float(Rem))}")
savename = "reconrate_{0:}_{1:}".format(int(float(Rem)), int(float(Re)))
plt.savefig(outputdir+savename+".png", bbox_inches="tight", dpi=300)
