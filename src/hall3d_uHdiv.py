1# -*- coding: utf-8 -*-
from firedrake import *

from datetime import datetime
import argparse
import numpy as np
import sys
import os
import itertools
import matplotlib.pyplot as plt
from pyop2.datatypes import IntType

import petsc4py
petsc4py.PETSc.Sys.popErrorHandler()

# Parallel distribution parameters
distribution_parameters = {"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}

# Definition of Burman Stabilisation term to avoid oscillations in velocity u
def BurmanStab(B, C, wind, stab_weight, mesh):
    n = FacetNormal(mesh)
    h = FacetArea(mesh)
    beta = avg(facet_avg(sqrt(inner(wind, wind)+1e-10)))
    gamma1 = stab_weight  # as chosen in doi:10.1016/j.apnum.2007.11.001
    stabilisation_form = 0.5 * gamma1 * avg(h)**2 * beta * inner(jump(grad(B), n), jump(grad(C), n))*dS
    return stabilisation_form

class SchurPC(AuxiliaryOperatorPC):
    def form(self, pc, V, U):
        [u, p] = split(U)
        [v, q] = split(V)

        A = (1./dt)*inner(u, v)*dx + dt*inner(grad(p), grad(q))*dx

        bcs = [DirichletBC(V.function_space().sub(0), 0, "on_boundary"),
               PressureFixBC(Z.sub(1), 0, 1)
               ]

        return (A, bcs)

class PressureFixBC(DirichletBC):
    def __init__(self, V, val, subdomain, method="topological"):
        super().__init__(V, val, subdomain, method)
        sec = V.dm.getDefaultSection()
        dm = V.mesh().topology_dm

        coordsSection = dm.getCoordinateSection()
        dim = dm.getCoordinateDim()
        coordsVec = dm.getCoordinatesLocal()

        (vStart, vEnd) = dm.getDepthStratum(0)
        indices = []
        for pt in range(vStart, vEnd):
            x = dm.getVecClosure(coordsSection, coordsVec, pt).reshape(-1, dim).mean(axis=0)
            if x.dot(x) == 0.0:  # fix [0, 0] in original mesh coordinates (bottom left corner)
                if dm.getLabelValue("pyop2_ghost", pt) == -1:
                    indices = [pt]
                break

        nodes = []
        for i in indices:
            if sec.getDof(i) > 0:
                nodes.append(sec.getOffset(i))

        if V.mesh().comm.rank == 0:
            nodes = [0]
        else:
            nodes = []
        self.nodes = np.asarray(nodes, dtype=IntType)


# LU solver
ICNTL_14 = 5000
lu = {"snes_type": "newtonls",
      "snes_monitor": None,
      "ksp_type": "preonly",
      "pc_type": "lu",
      "pc_factor_mat_solver_type": "mumps",
      "mat_mumps_icntl_14": 3000,
      "ksp_atol": 1.0e-15,
      "ksp_rtol": 1.0e-15
} 

lu_no_monitor = {"snes_type": "newtonls",
      "ksp_type": "preonly",
      "pc_type": "lu",
      "pc_factor_mat_solver_type": "mumps",
      "mat_mumps_icntl_14": 3000,
      "ksp_atol": 1.0e-15,
      "ksp_rtol": 1.0e-15
} 

solver_dict_main_krylov = {
    "snes_type": "newtonls",
    "ksp_type": "fgmres",
    "pc_type": "python",
    "pc_python_type": "__main__.SchurPC",
    "aux":{
        'ksp_type': 'cg',
        'pc_type': 'hypre',
        "ksp_atol": 1.0e-15,
        "ksp_rtol": 1.0e-15
    }
} 

cg_hypre = {"snes_type": "newtonls",
      "ksp_type": "cg",
      "pc_type": "hypre",
      "ksp_atol": 1.0e-15,
      "ksp_rtol": 1.0e-15
} 

gmres_ilu = {"snes_type": "newtonls",
      "ksp_type": "gmres",
      "pc_type": "ilu",
      "ksp_atol": 1.0e-13,
      "ksp_rtol": 1.0e-13
} 

# Definition of problem paramters
parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--baseN", type=int, default=10)
parser.add_argument("--k", type=int, default=3)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--Re", nargs='+', type=float, default=[1])
parser.add_argument("--Rem", nargs='+', type=float, default=[1])
parser.add_argument("--RHall", nargs='+', type=float, default=[1])
parser.add_argument("--gamma", type=float, default=10000)
parser.add_argument("--advect", type=float, default=1)
parser.add_argument("--dt", type=float, required=True)
parser.add_argument("--Tf", type=float, default=1)
parser.add_argument("--S", nargs='+', type=float, default=[1])
parser.add_argument("--solver-type", choices=["direct", "iterative"], required=True)
parser.add_argument("--stab", default=False, action="store_true")
parser.add_argument("--only-plot", default=False, action="store_true")
parser.add_argument("--output", default=False, action="store_true")

args, _ = parser.parse_known_args()
baseN = args.baseN
k = args.k
nref = args.nref
Re = Constant(args.Re[0])
Rem = Constant(args.Rem[0])
RHall = Constant(args.RHall[0])
gamma = Constant(args.gamma)
advect = args.advect
S = Constant(args.S[0])
dt = Constant(args.dt)
Tf = Constant(args.Tf)
stab = args.stab
solver_type = args.solver_type
only_plot = args.only_plot
output = args.output

# Stabilisation weight for BurmanStabilisation
stab_weight = Constant(3e-3)

base = UnitCubeMesh(baseN, baseN, baseN, distribution_parameters=distribution_parameters)
mh = MeshHierarchy(base, nref, reorder=True,
                       distribution_parameters=distribution_parameters)
mesh = mh[-1]

area = assemble(Constant(1, domain=mh[0])*dx)

def message(msg):
    if mesh.comm.rank == 0:
        warning(msg)

Vel = FiniteElement("N1div", mesh.ufl_cell(), k, variant="integral")
V = FunctionSpace(mesh, Vel)
Q = FunctionSpace(mesh, "DG", k-1)  # p
Rel = FiniteElement("N1curl", mesh.ufl_cell(), k, variant="integral")
R = FunctionSpace(mesh, Rel)  # E
Wel = FiniteElement("N1div", mesh.ufl_cell(), k, variant="integral")
W = FunctionSpace(mesh, Wel)
Jel = FiniteElement("N1curl", mesh.ufl_cell(), k, variant="integral")
JJ = FunctionSpace(mesh, Jel)  # j
HH = R
OO = R
UU = R
AA = R

Z = MixedFunctionSpace([V, Q, W, R, JJ, HH, OO, UU, AA])

# used for BurmannStabilisation
z_last_u = Function(V)

# time variable
t = Constant(0)
n = FacetNormal(mesh)

bcc = DirichletBC(R, 0, "on_boundary"),           

xx = SpatialCoordinate(Z.mesh())
u_pot = as_vector([1/pi*sin(pi*xx[1])*sin(pi*xx[2]),
                   1/pi*sin(pi*xx[0])*sin(pi*xx[2]),
                   1/pi*sin(pi*xx[0])*sin(pi*xx[1])])
u_ex = curl(u_pot)
a_ex = as_vector([1.0e-30*xx[0], 1.0e-30*xx[0], -sin(pi*xx[0])*sin(pi*xx[1])/pi])
B_ex = curl(a_ex)
p_ex = Constant(0, domain=mesh)
j_ex = curl(B_ex) #Constant((0, 0, 0), domain=mesh) #curl(B_ex)
H_ex = B_ex # Constant((0, 0, 0), domain=mesh) #as_vector([cos(y*zz), exp(x*zz), sinh(x)])
E_ex = 1/Rem*curl(B_ex) - cross(u_ex, H_ex) - RHall*cross(H_ex, j_ex) #Constant((0, 0, 0), domain=mesh)
w_ex = curl(u_ex)#Constant((0, 0, 0), domain=mesh) # curl(u_ex)
UU_ex = u_ex#Constant((0, 0, 0), domain=mesh)
alph_ex = cross(w_ex, UU_ex)#Constant((0, 0, 0), domain=mesh)

bcs = [DirichletBC(Z.sub(0), u_ex, "on_boundary"),
       DirichletBC(Z.sub(2), B_ex, "on_boundary"),
       DirichletBC(Z.sub(3), 0, "on_boundary"),
       DirichletBC(Z.sub(4), 0, "on_boundary"),           
       DirichletBC(Z.sub(5), 0, "on_boundary"),           
       DirichletBC(Z.sub(6), 0, "on_boundary"),           
       DirichletBC(Z.sub(7), 0, "on_boundary"),           
       DirichletBC(Z.sub(8), 0, "on_boundary"),           
       PressureFixBC(Z.sub(1), 0, 1)]

def form(z, z0, test_z, Z, timelevel):
    (uk, p, Bk, E, j, H, w, UU, alph) = split(z)
    (v, q, C, Ff, kk, G, w_, UU_, alph_) = split(test_z)
    (u0, p_, B0, E_, j_, H_, w__, UU__, alph__) = split(z0)
    u = (uk + u0)/2
    B = (Bk + B0)/2
    F = (
#      1/Re * inner(curl(u), curl(v))*dx
    + advect * inner(alph, v) * dx
    + gamma * inner(div(u), div(v)) * dx
    - inner(p, div(v)) * dx
    - inner(div(u), q) * dx
    + inner(j, kk) * dx
    - inner(B, curl(kk)) * dx
    + inner(H, G) * dx
    - inner(B, G) * dx
    + inner(w, w_) * dx
    - inner(u, curl(w_)) * dx
    + 1/Rem * inner(j, Ff) * dx
    - inner(E, Ff) * dx
    - inner(cross(UU, H), Ff) * dx
    + RHall * inner(cross(j, H), Ff) * dx
    + inner(curl(E), C) * dx
    + inner(alph, alph_)* dx
    - inner(cross(w, UU) + S*cross(j, H),alph_)*dx
    + inner(UU, UU_)*dx
    - inner(u, UU_)*dx
    )
    return F


def initial_condition():
    z = Function(Z)
    t.assign(0)
    z.sub(0).interpolate(u_ex)
    z.sub(1).interpolate(p_ex)
    z.sub(2).interpolate(B_ex)
    z.sub(3).interpolate(E_ex)
    z.sub(4).interpolate(j_ex)
    z.sub(5).interpolate(H_ex)
    z.sub(6).interpolate(w_ex)
    z.sub(7).interpolate(UU_ex)
    z.sub(8).interpolate(alph_ex)
    return z


# Initial Condition
z0 = initial_condition()
z = Function(z0)
z_test = TestFunction(Z)

F_paper = (
     inner(split(z)[0], split(z_test)[0])*dx
   - inner(split(z0)[0], split(z_test)[0])*dx
   + inner(split(z)[2], split(z_test)[2])*dx
   - inner(split(z0)[2], split(z_test)[2])*dx
   + dt*(form(z, z0, z_test, Z, 0))
  )

if stab:
    initial = interpolate(as_vector([sin(y), x, x]), V)
    z_last_u.assign(initial)
    stabilisation = BurmanStabilisation(Z.sub(0), state=z_last_u, h=FacetArea(mesh), weight=stab_weight)
    stabilisation_form_u = stabilisation.form(split(z)[0], split(z_test)[0])
    F_paper += dt*(advect * stabilisation_form_u)

# Set up nonlinear solver for direct solver
nvproblem_paper = NonlinearVariationalProblem(F_paper, z, bcs=bcs)
solver_paper = NonlinearVariationalSolver(nvproblem_paper, solver_parameters=lu)

results = {}
res = args.Re
rems = args.Rem
Ss = args.S
rhalls = args.RHall
pvd = File("output/mhd.pvd")

WW  = MixedFunctionSpace([V, Q])
u0_ex = curl(u_pot)
W_ex = as_vector([u_ex[0], u_ex[1], u_ex[2], p_ex])
bcw = [DirichletBC(WW.sub(0), u_ex, "on_boundary"),
       PressureFixBC(Z.sub(1), 0, 1)]
bcd = DirichletBC(W, 0, "on_boundary")
bcc = DirichletBC(R, 0, "on_boundary") 

params_project = cg_hypre
solver_dict_main = lu_no_monitor#solver_dict_main_krylov
# Parameters for iterative solver 
difftol = 1.E-12
maxit = 200 

# Compute magnetic helicity
def comp_mag_hel(u, B, E, j, H, w):
    a = TrialFunction(R) 
    fk = TestFunction(R) 
    sys = inner(curl(a),curl(fk))*dx 
    rhs = inner(B,curl(fk))*dx
    AA = Function(R)
    solve(sys == rhs, AA, bcc, solver_parameters=gmres_ilu)

    mag_hel = assemble(inner(AA, B)*dx)
    cross_hel = assemble(inner(u, B)*dx)
    alpha_ = RHall
    beta_ = RHall/S
    hybrid_hel = assemble(inner(AA+alpha_*u, B+beta_*w)*dx)
    energy = assemble(inner(u, u)*dx + S * inner(B, B)*dx)
    fluid_hel = assemble(inner(u, w)*dx)
    if mesh.comm.rank == 0:
        print(RED % ("Magnetic Helicity = %s" % mag_hel))
        print(RED % ("Cross Helicity = %s" % cross_hel))
        print(RED % ("Hybrid Helicity = %s" % hybrid_hel))
        print(RED % ("Energy = %s" % energy))

        global mag_hels, cross_hels, hybrid_hels, energys, fluid_hels
        mag_hels = np.append(mag_hels, abs(mag_hel))
        cross_hels = np.append(cross_hels, abs(cross_hel))
        hybrid_hels = np.append(hybrid_hels, abs(hybrid_hel))
        energys = np.append(energys, abs(energy))
        fluid_hels = np.append(fluid_hels, abs(fluid_hel))

def run(re, rem, s, rhall):
    z0.assign(initial_condition())
    z.assign(z0)

    (u, p, B, E, j, H, w, UU, alph) = z.split()
    (_, _, B0, _, _, _, _, _, _) = z0.split()
    (_, _, Bk, Ek, jk, Hk, wk, UUk, alphk) = Function(z).split()
    U = Function(WW)
    U.sub(0).interpolate(u_ex)
    U.sub(1).interpolate(p_ex)
    (u0,p0) = U.split() 
    U0 = Function(U) 
    Uk = Function(U)  
    Re.assign(re)
    Rem.assign(rem)
    S.assign(s)
    RHall.assign(rhall)

    # Set things up for timestepping
    T = args.Tf  # final time
    t.assign(0.0)  # current time we are solving for
    global dt
    ntimestep = 0  # number of timesteps solved
    total_nonlinear_its = 0  # number of total nonlinear iterations
    total_linear_its = 0  # number of total linear iterations

    while (float(t) < float(T-dt)+1.0e-10):
        t.assign(t+dt)
        if mesh.comm.rank == 0:
            print(BLUE % ("\nSolving for time: %f" % t), flush=True)
        comp_mag_hel(u, B, E, j, H, w)

        if mesh.comm.rank == 0:
            print(GREEN % ("Solving for #dofs = %s, Re = %s, Rem = %s, RHall = %s, gamma = %s, S = %s, baseN = %s, nref = %s, k = %s"
                           % (Z.dim(), float(re), float(rem), float(rhall), float(gamma), float(S), int(baseN), int(nref), int(float(k)))), flush=True)

        # Update z_last_u in Burman Stabilisation
        if stab:
            stabilisation.update(z.split()[0])
            z_last_u.assign(u)

        start = datetime.now()
        solver = solver_paper
        if solver_type == "direct":
            solver.solve()
        elif solver_type == "iterative":
            U0.assign(U)
            Uk.assign(U)  
            wk.assign(w)
            B0.assign(B)
            Bk.assign(B) 
            jk.assign(j)
            Hk.assign(H)
            Ek.assign(E)
            UUk.assign(UU)
            alphk.assign(alph)

        #   Nonlinear iteration 
            iter = 0    

            U0.assign(U) 
            (u0,p0) = U0.split()

            while iter in range(maxit):
                (uk,pk) = Uk.split()        

                wk = TrialFunction(R)
                kk = TestFunction(R)
                sys = inner(wk,kk)*dx
                rhs = inner((uk+u0)/2, curl(kk))*dx
                wk = Function(R)
                solve(sys == rhs, wk, bcc, solver_parameters=params_project)

                jk = TrialFunction(R)
                kk = TestFunction(R)
                sys = inner(jk,kk)*dx
                rhs = inner((Bk+B0)/2,curl(kk))*dx
                jk = Function(R)
                solve(sys == rhs, jk, bcc, solver_parameters=params_project)

                Hk = project((Bk+B0)/2,R,bcc,solver_parameters=params_project)

                UUk = project((uk+u0)/2,R,bcc,solver_parameters=params_project)

                Ek = project((1/Rem*jk - cross(UUk,Hk) + RHall*cross(jk,Hk)),R,bcc,solver_parameters=params_project)

                alphk = project(advect * cross(wk, UUk) - S * cross(jk, Hk),R,bcc,solver_parameters=params_project)

                B.assign(Bk)
                Bk = TrialFunction(W)
                C = TestFunction(W)
                sys = inner(Bk, C)*dx
                rhs = inner(B0 - dt*curl(Ek) , C)*dx
                Bk = Function(W)
                solve(sys == rhs, Bk, bcd, solver_parameters=lu_no_monitor)

                U = Function(WW)
                (u, p) = split(U)
                (v, q) = split(TestFunction(WW))

                A0 = (1./dt)*inner(u,v)*dx - inner(p, div(v))*dx - inner(q, div(u))*dx
                L0 = (
                    (1./dt)*inner(u0,v)*dx
                     - inner(alphk,v)*dx
                    )
                FFF = A0 -L0

                nv_problem = NonlinearVariationalProblem(FFF, U, bcs=bcw)
                solver = NonlinearVariationalSolver(nv_problem, solver_parameters=solver_dict_main, options_prefix="")

                solver.solve()

                (u, p) = U.split()        
                it_eps = sqrt(assemble(inner(uk-u,uk-u)*dx))/sqrt(assemble(inner(u,u)*dx))
                it_eps += sqrt(assemble(inner(Bk-B,Bk-B)*dx))/sqrt(assemble(inner(B, B)*dx))
                print(f"it_eps = {it_eps}", flush=True)
                iter += 1

                Uk.assign(U)

                if it_eps < difftol:
                    print(f"iter = {iter}")
                    break

            # Obtain most updated solution
            w.assign(wk) 
            B.assign(Bk)
            j.assign(jk)
            H.assign(Hk)
            E.assign(Ek)
            UU.assign(UUk)
            alph.assign(alphk)
        end = datetime.now()

        # Iteration numbers for this time step
        linear_its = solver.snes.getLinearSolveIterations()
        nonlinear_its = solver.snes.getIterationNumber()
        time = (end-start).total_seconds() / 60

        if nonlinear_its == 0:
            nonlinear_its = 1

        if linear_its == 0:
            linear_its = 1

        if mesh.comm.rank == 0:
            print(GREEN % ("Time taken: %.2f min in %d nonlinear iterations, %d linear iterations (%.2f Krylov iters per Newton step)"
                           % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its))), flush=True)
            print("%.2f @ %d @ %d @ %.2f" % (time, nonlinear_its, linear_its, linear_its/float(nonlinear_its)), flush=True)

        B.rename("MagneticField")
        u.rename("VelocityField")
        p.rename("Pressure")
        E.rename("ElectricFieldf")
        j.rename("CurrentDensity")
        H.rename("Hfield")
        w.rename("wfield")
        UU.rename("Ufield")
        alph.rename("alphafield")

        norm_div_u = sqrt(assemble(inner(div(u), div(u))*dx))
        norm_div_B = sqrt(assemble(inner(div(B), div(B))*dx))
        norm_div_j = sqrt(assemble(inner(div(j), div(j))*dx))
        norm_div_H = sqrt(assemble(inner(div(H), div(H))*dx))
        norm_div_w = sqrt(assemble(inner(div(w), div(w))*dx))
        norm_u = sqrt(assemble(inner(u, u)*dx))
        norm_B = sqrt(assemble(inner(B, B)*dx))
        norm_H = sqrt(assemble(inner(H, H)*dx))
        norm_j = sqrt(assemble(inner(j, j)*dx))
        norm_w = sqrt(assemble(inner(w, w)*dx))
        norm_UU = sqrt(assemble(inner(UU, UU)*dx))
        norm_alph = sqrt(assemble(inner(alph, alph)*dx))

        if mesh.comm.rank == 0:
            print("||div(u)||_L^2 = %s" % norm_div_u, flush=True)
            print("||div(B)||_L^2 = %s" % norm_div_B, flush=True)
            print("||div(j)||_L^2 = %s" % norm_div_j, flush=True)
            print("||div(H)||_L^2 = %s" % norm_div_H, flush=True)
            print("||div(w)||_L^2 = %s" % norm_div_w, flush=True)
            print("||u||_L^2 = %s" % norm_u, flush=True)
            print("||B||_L^2 = %s" % norm_B, flush=True)
            print("||H||_L^2 = %s" % norm_H, flush=True)
            print("||j||_L^2 = %s" % norm_j, flush=True)
            print("||w||_L^2 = %s" % norm_w, flush=True)
            print("||UU||_L^2 = %s" % norm_UU, flush=True)
            print("||alph||_L^2 = %s" % norm_alph, flush=True)
            global divBs, divus
            divBs = np.append(divBs, norm_div_B)
            divus = np.append(divus, norm_div_u)

        if output:
            pvd.write(u, p, B, E, j, H, w, time=float(t))

        global ts
        ts = np.append(ts, float(t))

        z_last_u.assign(u)

        # update time step
        z0.assign(z)
        ntimestep += 1
        total_nonlinear_its += nonlinear_its
        total_linear_its += linear_its

    # Calculate average number of nonlinear iterations per timestep and linear iterations per nonlinear iteration
    avg_nonlinear_its = total_nonlinear_its / float(ntimestep)
    avg_linear_its = total_linear_its / float(total_nonlinear_its)

    if mesh.comm.rank == 0:
        print(GREEN % ("Average %2.1f nonlinear iterations, %2.1f linear iterations"
                       % (avg_nonlinear_its, avg_linear_its)), flush=True)


mag_hel_dict = {}
div_B_dict = {}
div_u_dict = {}
cross_hel_dict = {}
hybrid_hel_dict = {}
energy_dict = {}
fluid_hel_dict = {}
legend_vec = []

output_dict = {
    "mag_hel": "magnetic helicity",
    "cross_hel": "cross helicity",
    "hybrid_hel": "hybrid helicity",
    "fluid_hel": "fluid helicity",
    "energy": "energy",
    "div_B": r"$\nabla \cdot \mathbf{B}$",
    "div_u": r"$\nabla \cdot \mathbf{u}$"
    }

plot_len = max(len(args.Re), len(args.RHall))
markers = itertools.cycle((',', '.', 'o', '*', '+', 's'))
linestyles = itertools.cycle(['--', '-.', '-', ':', (0, (5,1)), (0, (3, 1, 1, 1))])

from matplotlib import rc
rc('font', **{'family': 'serif', 'serif': ['Computer Modern'], 'size': 14})
rc('text', usetex=True)

def plot_quantity(my_dict, name, id, load_from_file=False):
    if mesh.comm.rank == 0:
            plt.figure(id)
            global legend_vec
            if load_from_file:
                my_dict = np.load(f"{name}_output.npy", allow_pickle=True)
                my_dict = my_dict[()]
                tss = np.linspace(0, float(Tf), int(1/float(dt)))
            else:
                global ts
                tss = ts[0:-1]
            for key in my_dict:
                if plot_len == 1:
                    legend_vec.append(output_dict[name])
                else:
                    legend_str = r"$\mathrm{Re} = \mathrm{Re_m} = \, $" if len(args.Re) > 1 else r"$\mathrm{R_H} = \,$ "
                    if float(key) > 1.0e10:
                        legend_vec.append(legend_str + r"$\infty$")
                    else:
                        legend_vec.append(legend_str + str(int(float(key))))
                vec = my_dict[key]
                #plt.plot(tss[1:], abs(vec[1:]), marker=next(markers), markevery=int(1/float(dt)/20))
                plt.plot(tss[1:], abs(vec[1:]), linestyle=next(linestyles))
            if len(args.RHall) == 1:
                tit = r"$\mathrm{R_H}" + f"= {float(RHall)}$"
                plt.title(tit)
            else:
                tit = r"$\mathrm{Re} = \mathrm{Re_m}= \infty$"
                plt.title(tit)
            plt.legend(legend_vec, loc = 'center right')
            plt.yscale("log")
            plt.xlabel(r'$t$')
            if plot_len > 1:
                plt.ylabel(output_dict[name])
            if plot_len == 1:            
                plt.ylim([1.0e-16, 10])
            plt.savefig(f"{name}_output.png", bbox_inches="tight", dpi=300)
            if not load_from_file:
                np.save(f"{name}_output.npy", my_dict, allow_pickle=True)
        
    
# Loop over parameters
if not only_plot:
    for rhall in rhalls:
        for rem in rems:
            for s in Ss:
                for re in res:
                    rem = re
                    print(RED % ("Re=Rem"))
                    mag_hels = np.array([])
                    cross_hels = np.array([])
                    hybrid_hels = np.array([])
                    divBs = np.array([])
                    divus = np.array([])
                    energys = np.array([])
                    fluid_hels = np.array([])
                    ts = np.array([0])

                    run(re, rem, s, rhall)

                    ind = re if len(args.Re)>1 else rhall
                    mag_hel_dict[ind] = mag_hels
                    div_B_dict[ind] = divBs
                    div_u_dict[ind] = divus
                    cross_hel_dict[ind] = cross_hels
                    hybrid_hel_dict[ind] = hybrid_hels
                    energy_dict[ind] = energys
                    fluid_hel_dict[ind] = fluid_hels
                    
if plot_len == 1:
    plot_quantity(mag_hel_dict, "mag_hel", 1, load_from_file=only_plot)
    plot_quantity(div_B_dict, "div_B", 1, load_from_file=only_plot)
    plot_quantity(div_u_dict, "div_u", 1, load_from_file=only_plot)
#    plot_quantity(cross_hel_dict, "cross_hel", 1, load_from_file=only_plot)
#    plot_quantity(hybrid_hel_dict, "hybrid_hel", 1, load_from_file=only_plot)
    plot_quantity(energy_dict, "energy", 1, load_from_file=only_plot)
#    plot_quantity(fluid_hel_dict, "fluid_hel", 1, load_from_file=only_plot)
else:
    plot_quantity(mag_hel_dict, "mag_hel", 1, load_from_file=only_plot)
    plot_quantity(div_B_dict, "div_B", 2, load_from_file=only_plot)
#    plot_quantity(cross_hel_dict, "cross_hel", 3, load_from_file=only_plot)
#    plot_quantity(hybrid_hel_dict, "hybrid_hel", 4, load_from_file=only_plot)
    plot_quantity(energy_dict, "energy", 5, load_from_file=only_plot)
#    plot_quantity(fluid_hel_dict, "fluid_hel", 6, load_from_file=only_plot)
    plot_quantity(div_u_dict, "div_u", 7, load_from_file=only_plot)
    
